// null & undefined
let age = null;

console.log(age, age + 3, `the age is ${age}`);

// undefined + 3 => Nan
// null + 3 => 3
