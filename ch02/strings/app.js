// strings
console.log("hello, world");

let email = "mario@thenetninja.co.uk";
console.log(email);

// string concatenation
let firstName = "Brandon";
let lastName = "Sanderson";

let fullName = firstName + " " + lastName;

console.log(fullName);

// getting individual characters
console.log(fullName[2]);

// string length
console.log(fullName.length);

// string methods
console.log(fullName.toUpperCase());
let result = fullName.toLocaleLowerCase();
console.log(result);

let index = email.indexOf("@");
console.log("index of the @ sign:", index);

// common string methods

let email = "mario@thenetninja.co.uk";

//let result = email.lastIndexOf('n');

//let result = email.slice(0,5);  // from, to

//let result = email.substr(5,12);

//let result = email.replace('m', 'w');

let result = email.replace("n", "w");

console.log(result);
