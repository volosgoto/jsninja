// for loops

// for(let i = 0; i < 5; i++){
//   console.log('loop: ', i);
// }

const names = ["sam", "dean", "rovena"];

for (let i = 0; i < names.length; i++) {
  let html = "";
  html += `<div>${names[i]}</div>`;
  console.log(html);
}
